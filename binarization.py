import os
from argparse import ArgumentParser

import cv2
import numpy as np
import skimage.morphology as morphology


def adjust_gamma(image, gamma=1.2):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")

    return cv2.LUT(image, table)

BLOCK_SIZE = 40
DELTA = 25

def preprocess(image):
    image = cv2.medianBlur(image, 3)
    return 255 - image

def postprocess(image):
    kernel = np.ones((3,3), np.uint8)
    image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
    return image

def get_block_index(image_shape, yx, block_size): 
    y = np.arange(max(0, yx[0]-block_size), min(image_shape[0], yx[0]+block_size))
    x = np.arange(max(0, yx[1]-block_size), min(image_shape[1], yx[1]+block_size))
    return np.meshgrid(y, x)

def adaptive_median_threshold(img_in):
    med = np.median(img_in)
    img_out = np.zeros_like(img_in)
    img_out[img_in - med < DELTA] = 255
    kernel = np.ones((3,3),np.uint8)
    img_out = 255 - cv2.dilate(255 - img_out,kernel,iterations = 2)
    return img_out

def block_image_process(image, block_size):
    out_image = np.zeros_like(image)
    for row in range(0, image.shape[0], block_size):
        for col in range(0, image.shape[1], block_size):
            idx = (row, col)
            block_idx = get_block_index(image.shape, idx, block_size)
            out_image[block_idx] = adaptive_median_threshold(image[block_idx])
    return out_image

def clean_edges(binarization_mask, connectivity=8):
  num_labels, labels_im = cv2.connectedComponents(
      binarization_mask.astype(np.uint8),
      connectivity=connectivity)

  for label in range(num_labels):
    if (labels_im[:, 0:5] == label).sum() > 0 \
        or (labels_im[:, -5:] == label).sum() > 0 \
        or (labels_im[0:5, :] == label).sum() > 0 \
        or (labels_im[-5:, :] == label).sum() > 0:
      binarization_mask[labels_im == label] = 0
  return binarization_mask

def process_image(img):
    image_in = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    image_in = preprocess(image_in)
    image_out = block_image_process(image_in, BLOCK_SIZE)
    # image_out = 255 - clean_edges(255 - image_out)
    image_out = postprocess(image_out)
    return image_out

def sigmoid(x, orig, rad):
    k = np.exp((x - orig) * 5 / rad)
    return k / (k + 1.)

def combine_block(img_in, mask):
    img_out = np.zeros_like(img_in)
    img_out[mask == 255] = 255
    fimg_in = img_in.astype(np.float32)

    idx = np.where(mask == 0)
    if idx[0].shape[0] == 0:
        img_out[idx] = img_in[idx]
        return img_out

    lo = fimg_in[idx].min()
    hi = fimg_in[idx].max()
    v = fimg_in[idx] - lo
    r = hi - lo

    img_in_idx = img_in[idx]
    ret3,th3 = cv2.threshold(img_in[idx],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    bound_value = np.min(img_in_idx[th3[:, 0] == 255])
    bound_value = (bound_value - lo) / (r + 1e-5)
    f = (v / (r + 1e-5))
    f = sigmoid(f, bound_value + 0.05, 0.2)

    img_out[idx] = (255. * f).astype(np.uint8)
    return img_out

def combine_block_image_process(image, mask, block_size):
    out_image = np.zeros_like(image)
    for row in range(0, image.shape[0], block_size):
        for col in range(0, image.shape[1], block_size):
            idx = (row, col)
            block_idx = get_block_index(image.shape, idx, block_size)
            out_image[block_idx] = combine_block(
                image[block_idx], mask[block_idx])
    return out_image

def combine_postprocess(image):
    return image

def combine_process(img, mask):
    image_in = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    image_out = combine_block_image_process(image_in, mask, 20)
    image_out = combine_postprocess(image_out)
    return image_out


if __name__ == '__main__':
  from tkinter import Tk, Label, Canvas, NW
  from PIL import ImageTk, Image
  from tkinter.filedialog import askopenfilename

  root = Tk()
  # Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
  filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
  print(filename)

  img_path = os.path.expanduser(filename)
  outpath = os.path.join(
      os.path.dirname(filename),
      'output_' + os.path.basename(filename))
  input_image = cv2.imread(img_path)
  image = adjust_gamma(input_image)
  mask = process_image(image)
  binarization = combine_process(image, mask)
  binarization = 255 - clean_edges(255 - binarization)

  vis = np.stack([binarization]*3, axis=-1)
  vis = np.concatenate([
    cv2.cvtColor(input_image,cv2.COLOR_BGR2RGB), vis], axis=1)


  img = Image.fromarray(vis)
  # PhotoImage class is used to add image to widgets, icons etc 
  # create a label 
  # panel = Label(root, image = img) 

  max_imheight = 600
  width, height = vis.shape[1], vis.shape[0]

  if not binarization.shape[0] < 600:
    width,height = int(width * (600. / height)), 600
  img = img.resize((width, height))
  img = ImageTk.PhotoImage(img) 
  canvas = Canvas(root, width = width, height = height)  
  canvas.pack()  
  # img = ImageTk.PhotoImage(Image.open("ball.png"))  
  canvas.create_image(0,0, anchor=NW, image=img) 
  root.mainloop() 
  # set the image as img  
  #panel.image = img 
  #panel.grid(row = 2) 
  root.mainloop()
  cv2.imwrite(outpath, binarization)